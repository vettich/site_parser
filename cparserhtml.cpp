#include "cparserhtml.h"
#include <map>
#include <iostream>

using namespace std;

CParserHtml::CParserHtml()
{
}

std::map<string, int> CParserHtml::parser(string text)
{
    map<string, int> result;
    int pos = -1;
    string tag;
    while((pos = get_next_tag_pos(text, pos)) >= 0)
    {
        tag = get_tag_name(text, pos);
        if(tag.empty())
            continue;
        result[tag]++;
    }
    return result;
}

string CParserHtml::get_tag_name(string text, int pos)
{
    string ret = "";
    for(int i=pos, len=text.length(); i<len; i++)
    {
        char ch = text[i];
        if(is_char_tag_name(ch))
        {
            ret += ch;
        }
        else
            break;
    }
    return ret;
}

/*
 * @return find next tag open position
 */
int CParserHtml::get_next_tag_pos(string text, int pos)
{
    int start = 0;
    if(pos >= 0)
        start = get_tag_close_pos(text, pos);
    size_t tag_open_pos = text.find('<', start);
    if(tag_open_pos != string::npos)
        return tag_open_pos + 1;
    return -1;
}

int CParserHtml::get_tag_close_pos(std::string text, int pos)
{
    size_t tag_close_pos = text.find('>', pos);
    if(tag_close_pos != string::npos)
        return tag_close_pos;
    return -1;
}


bool CParserHtml::is_char_tag_name(char ch)
{
    if(ch >= 'A' && ch <= 'z')
        return true;
    return false;
}
