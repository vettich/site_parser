TEMPLATE = app

QT += core
QT -= gui
QT += network

CONFIG += console
CONFIG -= app_bundle

SOURCES += main.cpp \
    cparserhtml.cpp \
    downloader.cpp


HEADERS += \
    cparserhtml.h \
    downloader.h

