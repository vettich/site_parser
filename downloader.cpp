#include "downloader.h"

#include <QtCore/QEventLoop>
#include <QtCore/QTimer>
#include <iostream>

Downloader::Downloader(QObject *parent) : QObject(parent)
{
    // Инициализируем менеджер ...
    manager = new QNetworkAccessManager();
}

std:: string Downloader::getData(QString surl)
{
    QUrl url(surl); // URL, к которому будем получать данные
    QNetworkRequest request;    // Отправляемый запрос
    request.setUrl(url);        // Устанавлвиваем URL в запрос
    QNetworkReply *reply = manager->get(request);
    while (reply->isRunning())
    {
        QEventLoop loop;
        QTimer::singleShot(100, &loop, SLOT(quit()));
        loop.exec();
    }
    if(reply->isFinished() && !reply->error())
    {
        return reply->readAll().data();
    }
    else
        return "";
}
