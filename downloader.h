#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <QtCore/QtCore>
#ifdef Q_WS_X11
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#endif // Q_OS_LINUX

#ifdef Q_WS_WIN
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#endif // Q_WS_WIN

class Downloader : public QObject
{
    Q_OBJECT
public:
    Downloader(QObject *parent = 0);

public slots:
    std::string getData(QString surl);     // Метод инициализации запроса на получение данных

private:
    QNetworkAccessManager *manager; // менеджер сетевого доступа
};

#endif // DOWNLOADER_H
