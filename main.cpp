#include <iostream>
#include <map>
#include <QCoreApplication>
#include "cparserhtml.h"
#include "downloader.h"

using namespace std;

#define DEF_URL "https://ya.ru"

int main(int argc, char *argv[])
{
    cout << "Парсер сайта (с)" << endl
         << "Разработчики: Леньшин Олег и Семенов Евгений" << endl << endl;

    QCoreApplication *app = new QCoreApplication(argc, argv);
    Downloader* downloader = new Downloader();
    string url = "";
    if(argc == 1)
    {
        cout << "URL по умолчанию: " << DEF_URL << endl;
        url = DEF_URL;
    }
    else if(argc == 2)
    {
        cout << "Введенное URL: " << argv[1] << endl;
        url = argv[1];
    }
    else
    {
        cout << "Вы ввели неверные параметры" << endl
             << "Пример использования:" << endl
             << "\t$ site_parser" << endl
             << "\t$ site_parser http://yandex.ru" << endl;
    }
    if(url.length() > 0)
    {
        QString surl = url.data();
        std::string data = downloader->getData(surl);
        map<string, int> tmap = CParserHtml::parser(data);
        cout << "Количество типов тегов: " << tmap.size() << endl;
        int total = 0;
        for(map<string, int>::iterator i = tmap.begin(); i != tmap.end(); ++i)
        {
            cout << (*i).first << ":\t\t" << (*i).second << endl;
            total += (*i).second;
        }
        cout << "Итого:\t" << total << endl;
    }
    app->exit();
}
