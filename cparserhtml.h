#ifndef CPARSERHTML_H
#define CPARSERHTML_H

#include <string>
#include <map>

class CParserHtml
{
public:
    CParserHtml();

    static std::map<std::string, int> parser(std::string text);
    static std::string get_tag_name(std::string text, int pos);
    static int get_next_tag_pos(std::string text, int pos);
    static int get_tag_close_pos(std::string text, int pos);
    static bool is_char_tag_name(char ch);
};

#endif // CPARSERHTML_H
